<?php

class SubObject
{
    static $instances = 0;
    public $instance;

    public function __construct() {
        $this->instance = ++self::$instances;
    }

    public function __clone() {
        $this->instance = ++self::$instances;
    }
}

class MyCloneable
{
    public $object1;
    public $object2;

    function __clone()
    {
        // Forzamos la copia de this->object, si no
        // hará referencia al mismo objeto.
        $this->object1 = clone $this->object1;
        $this->object2 = clone $this->object2;
    }
}

$obj = new MyCloneable();

$obj->object1 = new SubObject();
$obj->object2 = new SubObject();

$obj2 = clone $obj;


echo "Objeto 1: " . $obj->object1->instance . "<br>";
echo "Objeto 1: " . $obj->object2->instance . "<br>";
echo "Objeto 2: " . $obj2->object1->instance . "<br>";
echo "Objeto 2: " . $obj2->object2->instance;