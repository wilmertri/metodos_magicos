<?php

 /**
 * 
 */

 /**
 * 
 */
 class Entity
 {

 	private $name;
 	
 	public function __construct($name)
 	{
 		$this->name = $name;
 	}

 	public function getName()
 	{
 		return $this->name;
 	}
 }

 class Home extends Entity
 {

 	private $properties = array();
 	
 	public function __construct(array $properties=array(), $name)
 	{

 		
 		parent::__construct($name);

 	}

 	public function __toString()
 	{
 		return $this->address;
 	}

 	public function __get($name_property)
 	{
 		if (array_key_exists($name_property, $this->properties)) 
 		{
 			return $this->properties[$name_property];
 		}

 		return null;
 	}

 	public function __set($name_property, $valor)
 	{
 		
 			$this->properties[$name_property] = $valor;

 	}

 	public function __call($name, $arguments)
    {
        // Nota: el valor $name es sensible a mayúsculas.
        echo "Llamando al método de objeto '$name' "
             . implode(', ', $arguments). "\n";
    }
 }

$arreglo = array();
 $home = new Home($arreglo,'San Martin');

$home->address = "Cra 6 # 18 - 24";
$home->location = "Vereda 1";
$home->number_rooms = 4;

$home->met(1,"array");
