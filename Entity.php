<?php 

	require 'Inflector.php';
	/**
	* 
	*/
	class Entity
	{

		protected $properties; //En esta variables se guardarán todas las propiedades
		
		/**
		  Se utiliza el método mágico __construct para recibir las propiedades de una clase y
		  asignarlas a un array 
		*/

		public function __construct(array $values = array())
		{
			$this->properties = $values;
		}

		/**
      		este es el método mágico __get cuando se ejecute al llamar a una propiedad fuera del objeto
      		nosotros delegaremos la responsabilidad de devolver el valor de la propiedad al método getProperty
    	*/
		public function __get($name_property)
		{
			return $this->getProperty($name_property);
		}

		public function getProperty($name_property)
		{
			//Se utiliza la clase Inflector para retornar el valor en formato CamelCase
			
			$dynamicMethod = 'get' . Inflector::studly($name_property);

			//Verificamos si existe un metodo Dinamico para obtener la propiedad
			//Por ejemplo la propiedad numero_dias intentará llamar a getNumeroDias dentro del objeto
			if (method_exists($this, $dynamicMethod)) 
			{
				return $this->$dynamicMethod();	
			}

			//Si no existe el método se regresará el valor de la propiedad llamada
			//en caso de que exista dentro del array properties

			if (isset($this->properties[$name_property])) 
			{
				return $this->properties[$name_property];
			}

			return null;


		}
	}
?>