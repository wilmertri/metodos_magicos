<?php 

/**
* 
*/
class Inflector
{
	
	public static function studly($snakedString)
	{
		$array = explode('_', $snakedString); 	//La función explode divide un string en varios strings. Se determina por un delimitador.
		$array = array_map('ucfirst', $array); 	//La función array_map vuelve a llamar cada elemento de cada array y lo coloca en el mismo array segun el metodo aplicado.
		return implode('', $array); 			//La función implode une los elementos de un array en un string
	}
}


 