<?php 

/**
* 
*/
class User 
{
	private $name = "";
	private $email = "";
	private $telephone = "";
	
	function __construct($name)
	{
		$this->name = $name;
	}

	/**
	 ** Metodo __toString permite asignar una cadena string al objeto,
	 ** que sera mostrada si el objeto es llamado como una cadena
	 **/
	

	public function __toString()
	{
		return $this->name;
	}

	/**
	 ** Los metodos __set y __get permite acceder a los atributos de una clase
	 ** sin necesidad de crear un metodo por cada uno.
	 **/


	public function __get($var)
	{
		if (property_exists(__CLASS__,$var)) 
		{
			return $this->$var;
		}

		return NULL;
	}


	public function __set($var, $valor)
	{
		if (property_exists(__CLASS__, $var)) 
		{	
			$this->$var = $valor;
		}
		else
		{
			echo "El atributo al que intenta acceder no exite!";
		}
	}

	/**
	 ** El metodo __call es llamado cuando se trata de acceder a un metodo
	 ** que no existe o es inaccesible.
	 **/	

	public function __call($metodo, $parametros)
	{
		echo "<br>El metodo al que trata de acceder no existe!";
	}
}

$user = new User("Pablo de Tarso");

echo $user . "<br>";

$user->email = "pablo@gmail.com";

echo $user->email . "<br>";

$user->birthday = "19-10-1992";

$user->NoExiste();